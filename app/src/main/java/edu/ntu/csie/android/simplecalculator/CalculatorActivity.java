package edu.ntu.csie.android.simplecalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.text.DecimalFormat;

public class CalculatorActivity extends AppCompatActivity {
    //宣告多個Button的參考名稱
    private Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0
            ,btnPlus,btnMinus,btnMultiply,btnDibided
            ,btnEqual,btnDot,btnClean;
    //宣告一個TextView的參考名稱
    private TextView screenView;
    //宣告一個Double的參考名稱
    private Double oldNum;
    //宣告一個String的參考名稱
    private String calOperator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        //取得各個Button的ID
        btn1 = (Button)findViewById(R.id.btn_1);
        btn2 = (Button)findViewById(R.id.btn_2);
        btn3 = (Button)findViewById(R.id.btn_3);
        btn4 = (Button)findViewById(R.id.btn_4);
        btn5 = (Button)findViewById(R.id.btn_5);
        btn6 = (Button)findViewById(R.id.btn_6);
        btn7 = (Button)findViewById(R.id.btn_7);
        btn8 = (Button)findViewById(R.id.btn_8);
        btn9 = (Button)findViewById(R.id.btn_9);
        btn0 = (Button)findViewById(R.id.btn_0);
        btnDot = (Button)findViewById(R.id.btn_dot);
        btnClean = (Button)findViewById(R.id.btn_c);
        btnPlus = (Button)findViewById(R.id.btn_plus);// +
        btnMinus = (Button)findViewById(R.id.btn_minus);// -
        btnMultiply = (Button)findViewById(R.id.btn_multiply);// *
        btnDibided = (Button)findViewById(R.id.btn_divided);// /
        btnEqual= (Button)findViewById(R.id.btn_equal);  // =

        //取得TextView的ID
        screenView = (TextView)findViewById(R.id.screen_View);

        //取得TextView的ID
        btn1.setOnClickListener(Number);
        btn2.setOnClickListener(Number);
        btn3.setOnClickListener(Number);
        btn4.setOnClickListener(Number);
        btn5.setOnClickListener(Number);
        btn6.setOnClickListener(Number);
        btn7.setOnClickListener(Number);
        btn8.setOnClickListener(Number);
        btn9.setOnClickListener(Number);
        btn0.setOnClickListener(Number);
        btnDot.setOnClickListener(Number);
        btnPlus.setOnClickListener(Calculate);
        btnMinus.setOnClickListener(Calculate);
        btnMultiply.setOnClickListener(Calculate);
        btnDibided.setOnClickListener(Calculate);

        //實作btnClean並覆寫onClick功能
        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenView.setText("0");
            }
        });

        //實作btnEqual並覆寫onClick功能
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //從TextView取值
                double newNum = Double.parseDouble(screenView.getText().toString());
                //定義格式
                DecimalFormat nf = new DecimalFormat("0.00");
                //根據運算子實作相對運的計算，並將結果填入TextView
                if(calOperator.equals("+"))
                    screenView.setText(nf.format(oldNum+newNum));
                else if(calOperator.equals("-"))
                    screenView.setText(nf.format(oldNum-newNum));
                else if(calOperator.equals("x"))
                    screenView.setText(nf.format(oldNum*newNum));
                else if(calOperator.equals("/"))
                    screenView.setText(nf.format(oldNum/newNum));
            }
        });
    }

    //實作Number Method，從Button取值並回填TextView
    private Button.OnClickListener Number = new Button.OnClickListener(){
        @Override
        public void onClick(View v){
            //取得舊的輸入數字
            String oldKeyIn = screenView.getText().toString();
            if(oldKeyIn.equals("0"))
                oldKeyIn="";
            switch(v.getId()) {
                default:
                    //從Button取值
                    Button btn = (Button)findViewById(v.getId());
                    //與舊輸入數子做字串疊加，並回填TextView
                    screenView.setText(oldKeyIn+btn.getText().toString());
            }
        }
    };

    //實作Calculate Method，判別運算子
    private Button.OnClickListener Calculate = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            //當輸入運算子時，保留舊的輸入數字
            oldNum = Double.parseDouble(screenView.getText().toString());
            screenView.setText("");
            switch (v.getId()){
                case R.id.btn_plus:
                {
                    calOperator="+";
                    break;
                }
                case R.id.btn_minus:
                {
                    calOperator="-";
                    break;
                }
                case R.id.btn_multiply:
                {
                    calOperator="x";
                    break;
                }
                case R.id.btn_divided:
                {
                    calOperator="/";
                    break;
                }
            }

        }
    };
}
